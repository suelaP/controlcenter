🎉 Thank you for reporting this! 

**Version number**
Please state version here (found via the "i" info button top right of Web UI)

**Attach logs**
Consider attaching relevant mail logs to this issue - you can safely remove private data from them (email addresses, hostnames, IPs, etc) using the `utils/batch_log_cleaner` script in this repository. That creates clean log copies without modifying your source logs.
